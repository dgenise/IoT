#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"
#include <ESP8266WiFi.h>

#include "DHT.h"

#define AIO_SERVER      "io.adafruit.com"
#define AIO_SERVERPORT  1883
#define AIO_USERNAME  "AIO USER"
#define AIO_KEY  "AIO KEY"
WiFiClient client;

Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_USERNAME, AIO_KEY);

boolean MQTT_connect();

boolean MQTT_connect() {  int8_t ret; if (mqtt.connected()) {    return true; }  uint8_t retries = 3;  while ((ret = mqtt.connect()) != 0) { mqtt.disconnect(); delay(2000);  retries--;if (retries == 0) { return false; }} return true;}

DHT dht2(2,DHT11);

Adafruit_MQTT_Publish temperatura = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/temperatura");
Adafruit_MQTT_Publish humedad = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/humedad");
Adafruit_MQTT_Publish movimiento = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/movimiento");

//*********************** PIR Motion Sensor Setup *****************************                                                        
 

int inputPin = 13;                                          // choose the input pin (for PIR sensor) (Pin D6, GPIO 12)

void setup()
{
  Serial.begin(9600);
  WiFi.disconnect();
  delay(3000);
  Serial.println("START");
   WiFi.begin("WIFI USER","WIFI PASSWORD");
  while ((!(WiFi.status() == WL_CONNECTED))){
    delay(300);
    Serial.print("..");

  }
  Serial.println("Connected");
  Serial.println("Your IP is");
  Serial.println((WiFi.localIP().toString()));


 pinMode(inputPin, INPUT);                               // declare Motion sensor pin as input
}


void loop()
{

    if (MQTT_connect()) {
      //TEMPERATURA
      if (temperatura.publish((dht2.readTemperature( )))) {
        Serial.println("Enviada Temperatura");
         Serial.println(dht2.readTemperature( ));

      } else {
        Serial.println("Error al enviar temperatura");

      }
      //HUMEDAD
      if (humedad.publish((dht2.readHumidity( )))) {
        Serial.println("Enviado humedad");
        Serial.println(dht2.readHumidity( ));

      } else {
        Serial.println("Error al enviar humedad");

      }


      //PIR SENSOR
       movimiento.publish(digitalRead(inputPin));
        Serial.println("Valor PIR:");
        Serial.println(digitalRead(inputPin));



        long state = digitalRead(inputPin);
    if(state == HIGH) {
     
      Serial.println("Motion detected!");
      delay(1000);
    }
    else {
     
      Serial.println("Motion absent!");
      delay(1000);
      }
    
    delay(1500);

}}
